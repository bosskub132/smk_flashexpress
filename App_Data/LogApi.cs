﻿using SMKFlashExpress.App_Data;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

public class LogApi : DelegatingHandler
{
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        // log request body
        string requestBody = await request.Content.ReadAsStringAsync();
        requestBody = Regex.Replace(requestBody, @"\s+", " ");
        string logid = "";
        try
        {
            logid = saveLog("0", request.RequestUri.AbsoluteUri, request.RequestUri.AbsolutePath, request.Method.Method, requestBody, "");
        }
        catch
        {
            requestBody = new JavaScriptSerializer().Serialize(requestBody);
            logid = saveLog("0", request.RequestUri.AbsoluteUri, request.RequestUri.AbsolutePath, request.Method.Method, requestBody, "");
        }
        var result = await base.SendAsync(request, cancellationToken);

        if (result.Content != null)
        {

            var responseBody = await result.Content.ReadAsStringAsync();
            try
            {
                saveLog(logid,"", "", "", "", responseBody);
            }
            catch
            {
                responseBody = new JavaScriptSerializer().Serialize(responseBody);
                saveLog(logid, "","", "", "", responseBody);
            }
        }

        return result;
    }

    public string saveLog(string logid,string apiUrl, string apiName, string apiAction, string jsonReq, string jsonRes)
    {

        DataTable response = StoreQuery.LogApi_DataDetail(logid,apiUrl,apiName,apiAction,jsonReq,jsonRes);

        if (response.Rows.Count > 0)
        {
            return response.Rows[0]["log_id"].ToString();
        }
        else
        {
            return "";
        }

        
        
        /* string sql = "";
        if (logid == null || logid.Equals(""))
        {
            sql = " INSERT INTO UserLogDB.dbo.log_noncxp_callapi (api_name,api_action,json_request,sys_date)" +
            "  VALUES ('" + apiName + "' , '" + apiAction + "', '" + jsonReq + "',GETDATE())" +
            "  SELECT SCOPE_IDENTITY() AS logid";
        }
        else
        {
            sql = "UPDATE UserLogDB.dbo.log_noncxp_callapi SET json_response = '" + jsonRes + "' ,response_date = GETDATE() WHERE logid=" + logid;
        }*/

        /*DataTable dt = ConnDB.executeQueryDT(sql, con1);
        con1.Close();
        if (dt.Rows.Count > 0)
        {
            return dt.Rows[0]["logid"].ToString();
        }
        else
        {
            return "";
        }*/
    }
}