﻿


using System;
using System.Drawing;
using System.Drawing.Imaging;
using SMKFlashExpress.Models;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Configuration;

public class Util
{
    public static List<FilePath> Base64ToFile(List<string> encode,string tracking_no,string type)
    {
        //FilePath fp = new FilePath();
        List<FilePath> lfp = new List<FilePath>();
        DateTime datetime = DateTime.Now;
        string apiServer = ConfigurationManager.AppSettings["ApiServer"];
        //localpathEXhttp://192.168.1.161/fileshare/nonmot5/marine/flash_clam/ปี/เดือน/วันที่
        string physicalpath = String.Format(@"N:\nonmotdrive\nonmot5\marine\flash_claim\{0}\{1}\{2}", datetime.Year, datetime.Month, datetime.Day);
        string localpath = String.Format(@"http://"+apiServer+"/fileshare/nonmot5/marine/flash_claim/{0}/{1}/{2}", datetime.Year, datetime.Month, datetime.Day);
        //Example of base64 Data URI =>> data:image/jpg;base64,
        string pattern = @"data:(?<content>\w+)+\/+(?<type>\w+)+;base64,";
        //string status = "200";

        if (!Directory.Exists(physicalpath))
        {
            Directory.CreateDirectory(physicalpath);
        }
        for (int i = 1; i <= encode.Count; i++)
        {
            FilePath fp = new FilePath();
            string running_no = i.ToString("000");
            //Spilt base64 code into 4 part [0] = "" , [1] = <content> ,[2] = <type> , [3] = base64 code
            string[] arr = Regex.Split(encode[i - 1], pattern);
            string imgName = tracking_no+"_"+type+"-"+ running_no+"."+arr[2];
            string fullPath_p = System.IO.Path.Combine(physicalpath, imgName);
            string fullPath_l = localpath+"/"+imgName;
            if (!System.IO.File.Exists(fullPath_p))
            {
                
                byte[] databyte = Convert.FromBase64String(arr[3]);
                File.WriteAllBytes(fullPath_p, databyte);
                fp.type = type;
                fp.f_physicalpath = fullPath_p;
                fp.f_localpath = fullPath_l;
                lfp.Add(fp);

            }

        }
        return lfp;
        }
}

