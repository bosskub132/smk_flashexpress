﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMKFlashExpress.Models;

namespace SMKFlashExpress.App_Data
{
    public class StoreQuery
    {
        public static string constr = System.Configuration.ConfigurationManager.ConnectionStrings["SMKNonMotorConnectionString"].ConnectionString;

        public static DataTable RegisterServiceBigCargo_DataDetail(string json)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_reg_bigc", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@json", json);
                da.Fill(dt);
                conn.Close();
            }


            return dt;
        }

        public static DataTable ClaimServiceBigCargo_DataDetail(string json)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_clm_bigc", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@json", json);
                da.Fill(dt);
                conn.Close();
            }

            return dt;
        }

        public static DataTable CancelServiceBigCargo_DataDetail(string json)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_can_reg_clm", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@json", json);
                da.Fill(dt);
                conn.Close();
            }

            return dt;
        }

        public static DataTable RegisterServiceDeclareValue_DataDetail(string json)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_reg_decv", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@json", json);
                da.Fill(dt);
                conn.Close();
            }

            return dt;
        }

        public static DataTable ClaimServiceDeclareValue_DataDetail(string json)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_clm_decv", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@json", json);
                da.Fill(dt);
                conn.Close();
            }

            return dt;
        }

        public static DataTable CancelServiceDeclareValue_DataDetail(string json)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_can_reg_clm", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@json", json);
                da.Fill(dt);
                conn.Close();
            }

            return dt;
        }

        public static DataTable LogApi_DataDetail(string logid, string apiUrl, string apiName, string apiAction, string jsonReq, string jsonRes)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_ins_apiflash_log", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@logid", logid);
                cmd.Parameters.AddWithValue("@apiName", apiName);
                cmd.Parameters.AddWithValue("@apiAction", apiAction);
                cmd.Parameters.AddWithValue("@apiURL", apiUrl);
                cmd.Parameters.AddWithValue("@apiReq", jsonReq);
                cmd.Parameters.AddWithValue("@apiRes", jsonRes);
                da.Fill(dt);
                conn.Close();
            }

            return dt;
        }


        public static DataTable Check_Tracking(string tracking_no)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("mar_chk_flash_tracking", conn))
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                cmd.Parameters.AddWithValue("@pv_tracking", tracking_no);
                da.Fill(dt);
                conn.Close();
            }

            return dt;


        }

    }
}