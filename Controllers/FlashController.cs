﻿using System;
using System.Web.Http;
using System.Data;
using SMKFlashExpress.Models;
using SMKFlashExpress.App_Data;
using System.Web.Script.Serialization;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;

namespace SMKFlashExpress.Controllers
{
    public class FlashController : ApiController
    {
        
        //Check Bearer token
        private bool checkAuth()
        {
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            string authString = ConfigurationManager.AppSettings["AuthString"];
            string token = string.Empty;
            if (headers.Contains("Authorization"))
            {
                if (headers.Authorization.Scheme.Equals("Bearer") && headers.Authorization.Parameter != null)
                {
                    token = headers.Authorization.Parameter.ToString().Trim();
                    if (token.Equals(authString))
                    {
                        return true;
                    }
                    else
                    {
                        return false;

                    }

                }
                else
                    return false;
                
                
            }
            //code to authenticate and return some thing
            else
            { return false; }
        }



        //RegisterServiceBigCargo
        [Route("RegisterServiceBigCargo")]
        [HttpPost]
        public IHttpActionResult RegisterServiceBigCargo_POST([FromBody] RegisterService_request req_data)
        {
            if (!checkAuth())
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "001";
                dr.message = "Invalid token";
                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }

            if (req_data == null || !ModelState.IsValid)
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "400";
                dr.message = "BadRequest";
                dr.reason = "Required parameters";
                dr.tracking_no = req_data.tracking_no;
                dr.ref_no = "";

                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }
            try
            {
                var json = new JavaScriptSerializer().Serialize(req_data);

                DataTable responseData = StoreQuery.RegisterServiceBigCargo_DataDetail(json);
                if (responseData.Rows.Count > 0)
                {
                    RegisterAndClaim_response dr = new RegisterAndClaim_response();

                    dr.result = responseData.Rows[0]["result"].ToString().Trim();
                    dr.message = responseData.Rows[0]["message"].ToString().Trim();
                    dr.reason = responseData.Rows[0]["reason"].ToString().Trim();
                    dr.tracking_no = responseData.Rows[0]["tracking_no"].ToString().Trim();
                    dr.ref_no = responseData.Rows[0]["ref_no"].ToString().Trim();
                   

                    return Ok(dr);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }



        //ClaimServiceBigCargo
        [Route("ClaimServiceBigCargo")]
        [HttpPost]
        public IHttpActionResult ClaimServiceBigCargo_POST([FromBody] ClaimService_request req_data)
        {
            if (!checkAuth())
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "001";
                dr.message = "Invalid token";
                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }
            if (req_data == null || !ModelState.IsValid)
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "400";
                dr.message = "BadRequest";
                dr.reason = "Required parameters";
                dr.tracking_no = req_data.tracking_no;
                dr.ref_no = "";

                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }
            try
            {
                DataTable check_tracking = StoreQuery.Check_Tracking(req_data.tracking_no);
                
                if (check_tracking.Rows.Count > 0)
                {
                    RegisterAndClaim_response dr2 = new RegisterAndClaim_response();
                    dr2.result = check_tracking.Rows[0]["result"].ToString().Trim();
                    if (dr2.result == "000")
                    {
                        // Convert List<Obj> to List<string> and merge to Single List
                        //List<string> str_base64 = new List<string>();

                        if (req_data.doc_claim_f != null)
                        {
                            string type = "1";
                            List<string> tmp = new List<string>();
                            tmp = req_data.doc_claim_f.Select(i => i.claim_file.ToString()).ToList();
                            //tmp.ForEach(i => str_base64.Add(i));
                            req_data.file_path.AddRange(Util.Base64ToFile(tmp, req_data.tracking_no, type));

                        }
                        if (req_data.doc_show_product != null)
                        {
                            string type = "2";
                            List<string> tmp = new List<string>();
                            tmp = req_data.doc_show_product.Select(i => i.product_file.ToString()).ToList();
                            //tmp.ForEach(i => str_base64.Add(i));
                            req_data.file_path.AddRange(Util.Base64ToFile(tmp, req_data.tracking_no, type));


                        }
                        if (req_data.image != null)
                        {
                            string type = "3";
                            List<string> tmp = new List<string>();
                            tmp = req_data.image.Select(i => i.image_file.ToString()).ToList();
                            //tmp.ForEach(i => str_base64.Add(i));
                            req_data.file_path.AddRange(Util.Base64ToFile(tmp, req_data.tracking_no, type));

                        }

                        //req_data.file_path = Util.Base64ToFile(str_base64,req_data.tracking_no);


                        var json = new JavaScriptSerializer().Serialize(req_data);
                        DataTable responseData = StoreQuery.ClaimServiceBigCargo_DataDetail(json);
                        if (responseData.Rows.Count > 0)
                        {
                            RegisterAndClaim_response dr = new RegisterAndClaim_response();

                            dr.result = responseData.Rows[0]["result"].ToString().Trim();
                            dr.message = responseData.Rows[0]["message"].ToString().Trim();
                            dr.reason = responseData.Rows[0]["reason"].ToString().Trim();
                            dr.tracking_no = responseData.Rows[0]["tracking_no"].ToString().Trim();
                            dr.ref_no = responseData.Rows[0]["ref_no"].ToString().Trim();

                            return Ok(dr);
                        }
                        else
                        {
                            return BadRequest();
                        }

                    }
                    else 
                    {
                        dr2.result = check_tracking.Rows[0]["result"].ToString().Trim();
                        dr2.message = check_tracking.Rows[0]["message"].ToString().Trim();
                        dr2.reason = check_tracking.Rows[0]["reason"].ToString().Trim();
                        dr2.tracking_no = check_tracking.Rows[0]["tracking_no"].ToString().Trim();
                        dr2.ref_no = check_tracking.Rows[0]["ref_no"].ToString().Trim();

                        return Ok(dr2);
                    }
                    
                }
                else 
                {
                    return BadRequest();
                }
                
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        //CancelServiceBigCargo
        [Route("CancelServiceBigCargo")]
        [HttpPost]
        public IHttpActionResult CancelServiceBigCargo_POST([FromBody] CancelService_request req_data)
        {
            if (!checkAuth()) {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "001";
                dr.message = "Invalid token";
                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }


            if (req_data == null || !ModelState.IsValid)
            {
                DefaultResponse dr = new DefaultResponse();

                dr.result = "400";
                dr.message = "BadRequest";
                dr.reason = "Required parameters";
                dr.tracking_no = req_data.tracking_no;

                return Content(System.Net.HttpStatusCode.BadRequest, dr);

            }
            
            try
            {
                var json = new JavaScriptSerializer().Serialize(req_data);
                

                DataTable responseData = StoreQuery.CancelServiceBigCargo_DataDetail(json);
                if (responseData.Rows.Count > 0)
                {
                    DefaultResponse dr = new DefaultResponse();

                    dr.result = responseData.Rows[0]["result"].ToString().Trim();
                    dr.message = responseData.Rows[0]["message"].ToString().Trim();
                    dr.reason = responseData.Rows[0]["reason"].ToString().Trim();
                    dr.tracking_no = responseData.Rows[0]["tracking_no"].ToString().Trim();

                    return Ok(dr);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        //RegisterServiceDeclareValue
        [Route("RegisterServiceDeclareValue")]
        [HttpPost]
        public IHttpActionResult RegisterServiceDeclareValue_POST([FromBody] RegisterService_request req_data)
        {
            if (!checkAuth())
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "001";
                dr.message = "Invalid token";
                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }

            if (req_data == null || !ModelState.IsValid)
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "400";
                dr.message = "BadRequest";
                dr.reason = "Required parameters";
                dr.tracking_no = req_data.tracking_no;
                dr.ref_no = "";

                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }
            try
            {
                var json = new JavaScriptSerializer().Serialize(req_data);

                DataTable responseData = StoreQuery.RegisterServiceDeclareValue_DataDetail(json);
                if (responseData.Rows.Count > 0)
                {
                    RegisterAndClaim_response dr = new RegisterAndClaim_response();

                    dr.result = responseData.Rows[0]["result"].ToString().Trim();
                    dr.message = responseData.Rows[0]["message"].ToString().Trim();
                    dr.reason = responseData.Rows[0]["reason"].ToString().Trim();
                    dr.tracking_no = responseData.Rows[0]["tracking_no"].ToString().Trim();
                    dr.ref_no = responseData.Rows[0]["ref_no"].ToString().Trim();

                    return Ok(dr);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        //ClaimServiceDeclareValue
        [Route("ClaimServiceDeclareValue")]
        [HttpPost]
        public IHttpActionResult ClaimServiceDeclareValue_POST([FromBody] ClaimService_request req_data)
        {
            if (!checkAuth())
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "001";
                dr.message = "Invalid token";
                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }

            if (req_data == null || !ModelState.IsValid)
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "400";
                dr.message = "BadRequest";
                dr.reason = "Required parameters";
                dr.tracking_no = req_data.tracking_no;
                dr.ref_no = "";

                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }
            try
            {
                DataTable check_tracking = StoreQuery.Check_Tracking(req_data.tracking_no);

                if (check_tracking.Rows.Count > 0)
                {
                    RegisterAndClaim_response dr2 = new RegisterAndClaim_response();
                    dr2.result = check_tracking.Rows[0]["result"].ToString().Trim();
                    if (dr2.result == "000")
                    {
                        // Convert List<Obj> to List<string> and merge to Single List
                        //List<string> str_base64 = new List<string>();

                        if (req_data.doc_claim_f != null)
                        {
                            string type = "1";
                            List<string> tmp = new List<string>();
                            tmp = req_data.doc_claim_f.Select(i => i.claim_file.ToString()).ToList();
                            //tmp.ForEach(i => str_base64.Add(i));
                            req_data.file_path.AddRange(Util.Base64ToFile(tmp, req_data.tracking_no, type));

                        }
                        if (req_data.doc_show_product != null)
                        {
                            string type = "2";
                            List<string> tmp = new List<string>();
                            tmp = req_data.doc_show_product.Select(i => i.product_file.ToString()).ToList();
                            //tmp.ForEach(i => str_base64.Add(i));
                            req_data.file_path.AddRange(Util.Base64ToFile(tmp, req_data.tracking_no, type));


                        }
                        if (req_data.image != null)
                        {
                            string type = "3";
                            List<string> tmp = new List<string>();
                            tmp = req_data.image.Select(i => i.image_file.ToString()).ToList();
                            //tmp.ForEach(i => str_base64.Add(i));
                            req_data.file_path.AddRange(Util.Base64ToFile(tmp, req_data.tracking_no, type));

                        }

                        //req_data.file_path = Util.Base64ToFile(str_base64,req_data.tracking_no);


                        var json = new JavaScriptSerializer().Serialize(req_data);
                        DataTable responseData = StoreQuery.ClaimServiceDeclareValue_DataDetail(json);
                        if (responseData.Rows.Count > 0)
                        {
                            RegisterAndClaim_response dr = new RegisterAndClaim_response();

                            dr.result = responseData.Rows[0]["result"].ToString().Trim();
                            dr.message = responseData.Rows[0]["message"].ToString().Trim();
                            dr.reason = responseData.Rows[0]["reason"].ToString().Trim();
                            dr.tracking_no = responseData.Rows[0]["tracking_no"].ToString().Trim();
                            dr.ref_no = responseData.Rows[0]["ref_no"].ToString().Trim();

                            return Ok(dr);
                        }
                        else
                        {
                            return BadRequest();
                        }

                    }
                    else 
                    {
                        dr2.result = check_tracking.Rows[0]["result"].ToString().Trim();
                        dr2.message = check_tracking.Rows[0]["message"].ToString().Trim();
                        dr2.reason = check_tracking.Rows[0]["reason"].ToString().Trim();
                        dr2.tracking_no = check_tracking.Rows[0]["tracking_no"].ToString().Trim();
                        dr2.ref_no = check_tracking.Rows[0]["ref_no"].ToString().Trim();

                        return Ok(dr2);
                    }
                    
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //CancelServiceDeclareValue
        [Route("CancelServiceDeclareValue")]
        [HttpPost]
        public IHttpActionResult CancelServiceDeclareValue_POST([FromBody] CancelService_request req_data)
        {
            if (!checkAuth())
            {
                RegisterAndClaim_response dr = new RegisterAndClaim_response();
                dr.result = "001";
                dr.message = "Invalid token";
                return Content(System.Net.HttpStatusCode.BadRequest, dr);
            }

            if (req_data == null || !ModelState.IsValid)
            {
                DefaultResponse dr = new DefaultResponse();

                dr.result = "400";
                dr.message = "BadRequest";
                dr.reason = "Required parameters";
                dr.tracking_no = req_data.tracking_no;

                return Content(System.Net.HttpStatusCode.BadRequest, dr);

            }
            try
            {
                var json = new JavaScriptSerializer().Serialize(req_data);

                DataTable responseData = StoreQuery.CancelServiceDeclareValue_DataDetail(json);
                if (responseData.Rows.Count > 0)
                {
                    DefaultResponse dr = new DefaultResponse();

                    dr.result = responseData.Rows[0]["result"].ToString().Trim();
                    dr.message = responseData.Rows[0]["message"].ToString().Trim();
                    dr.reason = responseData.Rows[0]["reason"].ToString().Trim();
                    dr.tracking_no = responseData.Rows[0]["tracking_no"].ToString().Trim();

                    return Ok(dr);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}