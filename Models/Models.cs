﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMKFlashExpress.Models
{
    public class DefaultRequest
    {
        [Required] public string tracking_no { get; set; }
    }

    public class DefaultResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public string reason { get; set; }
        public string tracking_no { get; set; }
       

    }

    public class RegisterAndClaim_response
    {
        public string result { get; set; }
        public string message { get; set; }
        public string reason { get; set; }
        public string tracking_no { get; set; }
        public string ref_no { get; set; }
    }


    public class RegisterService_request : DefaultRequest
    {
        public string cus_id { get; set; }
        public string name { get; set; }
        [Required] public string cat_type { get; set; }
        public string desc { get; set; }
        [Required] public string date_receive_product { get; set; }
        [Required] public string province_receive { get; set; }
        [Required] public string zipcode_receive { get; set; }
        [Required] public string province_deliver { get; set; }
        [Required] public string zipcode_deliver { get; set; }
        [Required] public string price_declare { get; set; }
        [Required] public string service_dvs { get; set; }
        [Required] public string server_fs_weight { get; set; }
        [Required] public string server_fs_length { get; set; }
        [Required] public string server_fs_width { get; set; }
        [Required] public string server_fs_height { get; set; }
        [Required] public string titlename { get; set; }
        [Required] public string fname { get; set; }
        [Required] public string lname { get; set; }
        [Required] public string type_customer { get; set; }
        public string com_name { get; set; }

    }

    

    public class ClaimService_request : DefaultRequest
    {
        [Required] public string date_claim { get; set; }
        public string branch_clain { get; set; }
        [Required] public string code_type_damage { get; set; }
        public string detail_claim { get; set; }
        public List<DocClaimF> doc_claim_f { get; set; }
        public List<ShowProduct> doc_show_product { get; set; }
        [Required] public string claim_damage { get; set; }
        public List<Image> image { get; set; }
        public string desc { get; set; }
        public List<FilePath> file_path { get; set; } = new List<FilePath>();
        
    }

    


    public class CancelService_request : DefaultRequest
    {
        [Required] public string claim_status { get; set; }

    }

    /*public class RegisterServiceDeclareValue_request : DefaultRequest
    {
        public string cus_id { get; set; }
        public string name { get; set; }
        [Required] public string cat_type { get; set; }
        public string desc { get; set; }
        [Required] public string date_receive_product { get; set; }
        [Required] public string province_receive { get; set; }
        [Required] public string zipcode_receive { get; set; }
        [Required] public string province_deliver { get; set; }
        [Required] public string zipcode_deliver { get; set; }
        [Required] public string price_declare { get; set; }
        [Required] public string service_dvs { get; set; }
        [Required] public string server_fs_weight { get; set; }
        [Required] public string server_fs_length { get; set; }
        [Required] public string server_fs_width { get; set; }
        [Required] public string server_fs_height { get; set; }
        [Required] public string titlename { get; set; }
        [Required] public string fname { get; set; }
        [Required] public string lname { get; set; }
        [Required] public string type_customer { get; set; }
        public string com_name { get; set; }
    }*/

    /*public class ClaimServiceDeclareValue_request : DefaultRequest
    {
        [Required] public string date_claim { get; set; }
        public string branch_clain { get; set; }
        [Required] public string code_type_damage { get; set; }
        public string detail_claim { get; set; }
        public List<DocClaimF> doc_claim_f { get; set; }
        public List<ShowProduct> doc_show_product { get; set; }
        [Required] public string claim_damage { get; set; }
        public List<Image> image { get; set; }
        public string desc { get; set; }
    }*/


    /*public class CancelServiceDeclareValue_request : DefaultRequest
    {
        [Required] public string claim_status { get; set; }
    
    }*/


    public class DocClaimF
    {
        public string claim_file { get; set; }
    }

    public class ShowProduct
    {
        public string product_file { get; set; }
    }

    public class Image
    {
        public string image_file { get; set; }
    }

    public class FilePath 
    {
        public string type { get; set; }
        public string f_physicalpath { get; set; }
        public string f_localpath { get; set; }
    
    }



}